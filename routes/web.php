<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PruebaController;
use App\Http\Controllers\PhotoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('tabla/{size}', [PruebaController::class, 'tabla']);

Route::get('prueba', function() {
    echo "Hola mundo";
});

//Route::get('prueba2/{$name}', PruebaController::class);
Route::resource('photos', PhotoController::class);
//Route::resource('/photos/create', [PhotoController::class, 'create']);
//Route::post('/photos', [PhotoController::class, 'store']);
//Route::get('/photos/', [PhotoController::class, 'index']);
//Route::get('/photos/{name}/edit', [PhotoController::class, 'edit']);
//Route::get('/photos/{name}', [PhotoController::class, 'show']);
//Route::put('/photos/{name}', [PhotoController::class, 'update']);
//Route::delete('/photos/{name}', [PhotoController::class, 'delete']);


//ejercicio crear la ruta tabla 
//debe ejecutar el metodo tabla de prueba controller 
//ese metodo debera mostrar una vista llamada prueba/tabla.blade.php 
//debe crear un tamano de 5x5



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
