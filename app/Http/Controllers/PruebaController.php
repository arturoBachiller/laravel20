<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PruebaController extends Controller
{
    public function hola()
    {
        return "Hola desde el prueba controller indicamos la ruta de la url y luego el controllador con su metodo";
    }
    public function saludo(Request $request)
    {
        //>” y “<
        $informal = $request->input('informal');
        $informal = $request->informal;
        $all = $request->all();
        //if($informal)
        return view('prueba.saludo');
    }

    public function tabla($size)
    {
        return view('prueba.tabla');
    }
}

