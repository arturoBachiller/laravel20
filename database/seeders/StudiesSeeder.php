<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StudiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //lo mismo con query builder
        DB::table('studies')->insert([
            'code' => 'IFC303';
            'name' => 'Desarrollo de aplicaciones web';
            'abreviation' => 'DAW';
        ]);
    }
}
